#include <stdio.h>
#include <math.h>

int main ()
{

	/*
	 * PLEASE NOTE: Many of the numbers in this program, such as that
	 * given for ARRAY_LENGTH, may seem arbitrary, but they are there for a
	 * reason, namely that they are the maximum values that can be dealt
	 * with on most systems. The program either 1) won't work, 2) will
	 * work less efficiently, or 3) will just not do anything if you try to
	 * alter these numbers.
	 */

	int ARRAY_LENGTH = 208008;
	int primes[ARRAY_LENGTH];
	primes[0] = 2;
	primes[1] = 3;
	int i;
	int j;
	int current = 2;
	int runner = 0;
	short int prime = 1;

	for (i = 5; i < 2868161; ) {
		for (j = 3; j <= sqrt(i); ) {
			j = primes[runner];
			if (i % j == 0.0) {
				prime = 0;
				break;
			}
			runner++;
		}
		if (prime == 1) {
			printf("%d \n", i);
			primes[current] = i;
			current++;
		}
		prime = 1;
		i += 2;
		runner = 0;
	}

	printf("%d primes found.\n\n", current);
}
